defmodule Eldir.MixProject do
  use Mix.Project

  def project do
    [
      app: :eldir,
      version: "0.1.0",
      elixir: "~> 1.5",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix, :gettext] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {Eldir.Application, []},
      extra_applications: [:logger, :runtime_tools, :fs]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:phoenix, "~> 1.4.0"},
      {:phoenix_pubsub, "~> 1.1"},
      {:phoenix_html, "~> 2.11"},
      {:phoenix_live_reload, "~> 1.2", only: :dev},
      {:gettext, "~> 0.11"},
      {:jason, "~> 1.0"},
      {:plug_cowboy, "~> 2.0"},
      {:fs, github: "synrc/fs", manager: :rebar, override: true},
      {:earmark, "~> 1.3"},
      {:timex, "~> 3.6.1"},
      {:yaml_front_matter, "~> 1.0.0"},
      {:flow, "~> 0.14"},
      {:toml, "~> 0.5.2"},
      {:file_info, "~> 0.0.4"},
      {:git_cli, "~> 0.3"}
    ]
  end
end
