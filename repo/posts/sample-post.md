---
title: Sample Post
date: 2019-07-22
categories: 
- sample
- eldir
- blog
- instructions
authors: 
- owen
slug: sample-post
intro: This is a sample post.  It has instructions for how to use this application.
custom_field: This is my custom field
---
This is a sample post.  It will have instructions for how to use this application when I get around to writing them.