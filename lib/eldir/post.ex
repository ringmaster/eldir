require Logger
require IEx

defmodule Eldir.Post do
  defstruct file_name: "", slug: "", authors: [], title: "", date: "", intro: "", categories: [], meta: %{}, content: "", markdown: ""

  def transform(file) do
    case do_transform(file) do
      {:ok, post} -> {:ok, post}
      {:error, _post, reason} ->
        Logger.warn "Failed to compile #{file}, #{reason}"

        {:error, struct(Post)}
    end
  end

  defp cat_errors({_severity, line, error}, acc) do
    acc <> "Error at line #{line}: #{error}\n"
  end

  defp do_transform(file) do
    {:ok, matter, markdown} = split_frontmatter(file)
    case Earmark.as_html(markdown, %Earmark.Options{pedantic: false, sanitize: true}) do
      {:ok, html, _} -> {:ok, into_post(file, matter, html, markdown)}
      {:error, html, error_messages} ->
        errors = error_messages |> Enum.reduce(html, &cat_errors/2)
        # TODO: This COULD return :error if there's an actual error, but right now :error is returned for :warning's which is not an error
        {:ok, into_post(file, matter, html <> "<!--" <> errors <> "-->")}
    end
  end

  defp split_frontmatter(file) do
    with{:ok, matter, body} <- parse_yaml_frontmatter(file),
        {:ok, matter, body} <- post_intro(matter, body),
        {:ok, parsed_date} <- Timex.parse(matter.date, "{ISOdate}"),
    do: {:ok, %{matter | date: parsed_date}, body}
  end

  defp parse_yaml_frontmatter(file) do
    case YamlFrontMatter.parse_file(file) do
      {:ok, matter, body} ->
        {:ok, Map.new(matter, fn {k, v} -> {String.to_atom(k), v} end), body}
      {:error, reason} ->
        {:error, reason}
    end
  end

  defp post_intro(matter, body) do
    if(Map.has_key?(matter, :intro)) do
      {:ok, matter, body}
    else
      [intro | _] = String.split(body, ~r{[\n\r]+})

      {:ok, Map.put(matter, :intro, intro), body}
    end
  end

  defp file_to_slug(file) do
    file |> Path.basename(file) |> String.replace(~r/\.md$/, "") |> URI.encode
  end

  defp into_post(file, meta, html, markdown \\ "") do
    categories = case Map.get(meta, :categories, nil) do
      [] -> ["untagged"]
      nil -> ["untagged"]
      categories -> categories
    end
    meta = Map.put(meta, :categories, categories)
    data = %{
      file_name: file,
      slug: file_to_slug(file),
      content: html,
      meta: meta,
      markdown: markdown
    } |> Map.merge(meta)

    struct(Eldir.Post, data)
  end
end
