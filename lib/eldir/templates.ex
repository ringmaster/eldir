require Logger
require IEx
require EEx

defmodule Eldir.Templates do
  use GenServer

  def start_link(options) do
    GenServer.start_link(__MODULE__, options, name: __MODULE__)
  end

  @impl true
  def init(options) do
    options |> get_initial_state

    :ok = watch(options[:source])

    {:ok, options}
  end

  @impl true
  def handle_call(:value, _from, state) do
    {:reply, state, state}
  end

  ## Initialize
  def get_initial_state(options) do
    start = Timex.now()

    # New Method
    templates = read_all_routes(options[:routes])
    ++ read_all_templates(options[:source])
    ++ add_module_defs()

    Module.create(Eldir.Templates.Current, templates, Macro.Env.location(__ENV__))

    # Logging, construction complete
    Logger.debug("Compiled #{Enum.count(templates)} templates in #{Timex.diff Timex.now(), start, :milliseconds}ms.")
    templates
  end

  def add_module_defs() do
    moduledefs = quote do
      require Eldir.Templates
      require Logger
      def render(conn, layout, assigns) do
        template = assigns[:template]
        Logger.debug("Rendering template #{template} in #{layout}")
        response = render(layout, assigns)
        conn
        |> Plug.Conn.put_resp_header("content-type", "text/html")
        |> Plug.Conn.send_resp(200, response)
      end
    end
    [{:head, moduledefs}]
  end

  def read_all_routes(routes) do
    routes
    |> Map.values()
    |> Enum.map(fn(route) ->
      path = String.split(route["route"], "/", trim: true)
      |> Enum.map(fn(segment) ->
        case segment do
          ":" <> rest ->
            {String.to_atom(rest), [line: 1], nil}
          value ->
            value
        end
      end)

      path_parts = String.split(route["route"], "/", trim: true)
      |> Enum.map(fn(segment) ->
        case segment do
          ":" <> rest ->
            String.to_atom(rest)
          _ ->
            nil
        end
      end)

      route
      |> Map.put(:path, path)
      |> Map.put(:path_parts, path_parts)
      |> route_quote()
    end)
  end

  def assign_post(assigns) do
    Keyword.put(assigns, :post,
      if(Keyword.has_key?(assigns[:path_parts], :slug)) do
        case Eldir.PostRepo.get_by_slug(assigns[:path_parts][:slug]) do
          {:ok, post} -> post
          _ -> %Eldir.Post{}
        end
      else
        %Eldir.Post{}
      end
    )
  end

  def assign_posts(assigns) do
    assigns
    |> (fn(reassigns) ->
      {:ok, posts} = cond do
        Keyword.has_key?(assigns[:path_parts], :slug) -> {:ok, []}
        Keyword.has_key?(assigns[:path_parts], :category) -> Eldir.PostRepo.get_by_category(assigns[:path_parts][:category])
        Keyword.has_key?(assigns[:path_parts], :author) -> Eldir.PostRepo.get_by_author(assigns[:path_parts][:author])
        true -> {:ok, Eldir.PostRepo.list()}
      end
      Keyword.put(reassigns, :posts, posts)
    end).()
    |> (fn(reassigns) ->
      paginate = Map.get(assigns[:route], "paginate", assigns[:config][:paginate])
      Keyword.put(reassigns, :paginate, paginate)
    end).()
    |> (fn(reassigns) ->
      Keyword.put(reassigns, :pages,
        ceil(Enum.count(reassigns[:posts]) / reassigns[:paginate])
      )
    end).()
    |> (fn(reassigns) ->
      Keyword.put(reassigns, :posts,
        Eldir.PostRepo.page(reassigns[:posts], assigns[:page] - 1, reassigns[:paginate])
      )
    end).()
  end

  def static_route(conn, options, route, _assigns, path) do
    filename = Enum.find([
      Keyword.get(Enum.zip(route[:path_parts], path), :file),
      route["file"],
      "FILE NOT PROVIDED VIA ROUTE"
    ], fn(x) -> nil != x end)

    file = Path.join([options[:source], route["dir"], filename])
    |> IO.inspect()

    if File.exists?(file) do
      filetype = FileInfo.get_info(file)
      mimetype = filetype[file].type <> "/" <> filetype[file].subtype
      conn
      |> Plug.Conn.put_resp_content_type(mimetype)
      |> Plug.Conn.send_file(200, file)
    else
      Plug.Conn.send_resp(conn, 404, "Requested file does not exist")
    end
  end

  defp route_quote(route = %{"type" => "static"}) do
    Logger.info("Static")

    IO.inspect(route)

    r = Macro.escape(route)

    quote do
      require Logger
      def route(conn, path=[unquote_splicing(route.path)], assigns) do

        route = unquote(r)
        Logger.info("Executing route: " <> route["route"])
        options = GenServer.call(Eldir.Templates, :value)

        Eldir.Templates.static_route(conn, options, route, assigns, path)
      end
    end
  end

  def update_route(conn, options, _route, _assigns, _path) do
    repo = Git.new options[:source]
    case Git.pull(repo) do
      {:ok, result} -> Plug.Conn.send_resp(conn, 200, result)
      {:error, error} -> Plug.Conn.send_resp(conn, 500, error.message)
    end
  end

  defp route_quote(route = %{"type" => "update"}) do
    Logger.info("Update")

    IO.inspect(route)

    r = Macro.escape(route)

    quote do
      require Logger
      def route(conn, path=[unquote_splicing(route.path)], assigns) do

        route = unquote(r)
        Logger.info("Executing route: " <> route["route"])
        options = GenServer.call(Eldir.Templates, :value)

        Eldir.Templates.update_route(conn, options, route, assigns, path)
      end
    end
  end

  defp route_quote(route) do
    Logger.info("Template")

    IO.inspect(route)

    r = Macro.escape(route)

    quote do
      require Logger
      def route(conn, path=[unquote_splicing(route.path)], assigns) do
        route = unquote(r)
        Logger.info("Executing route: " <> route["route"])

        path_parts = Enum.zip(route[:path_parts], path)

        new_assigns = assigns
        |> Keyword.put(:path, path)
        |> Keyword.put(:template, route["template"])
        |> Keyword.put(:path_parts, path_parts)
        |> Keyword.put(:route, route)
        |> Eldir.Templates.assign_posts()
        |> Eldir.Templates.assign_post()

        render(conn, unquote(route["layout"]), new_assigns)
      end
    end
  end

  def read_all_templates(source) do
    File.ls!(source)
    |> Enum.filter(fn(filename) -> Path.extname(filename) == ".eex" end)
    |> Enum.map(fn(filename) -> template_quote(Path.join(source, filename)) end)
  end

  defp template_quote(pathname) do
    info = [file: pathname, line: 1]
    compiled = EEx.compile_file(pathname, info)
    fname = pathname |> Path.basename(".eex")
    args = [fname, {:assigns, [line: 1], nil}]

    quote do
      def render(unquote_splicing(args)) do
        require Logger
        require Eldir.Templates
        unquote(compiled)
      end
    end
  end

  def load(filename, name \\ :render, args \\ [:assigns], options \\ []) do
    info = Keyword.merge(options, [file: filename, line: 1])
    args = Enum.map(args, fn arg -> {arg, [line: 1], nil} end)
    compiled = EEx.compile_file(filename, info)

    f = quote do
      require EEx
      defmodule Eldir.Templates.Current do
        def unquote(name)(unquote_splicing(args)), do: unquote(compiled)
      end
    end

    Code.compile_quoted(f)
  end

  def render(conn, template, assigns) do
    Logger.debug("Rendering template #{template}")
    response = apply(Eldir.Templates.Current, String.to_atom(template), [assigns])
    conn
    |> Plug.Conn.put_resp_header("content-type", "text/html")
    |> Plug.Conn.send_resp(200, response)
  end

  ## Watching function message handling
  def watch(source) do
    Logger.info("Watching #{source} for template updates...")
    name = to_string(__MODULE__) <> ".FS" |> String.to_atom

    {:ok, _pid} = :fs.start_link(name, source)
    :fs.subscribe(name)
  end

  def handle_info({_pid, {:fs, :file_event}, {path, events}}, source) do
    if Path.extname(path) == ".eex" do
      Logger.debug(Enum.join(Enum.map(events, fn i -> Atom.to_string(i) end), "+") <>  " change detected in #{path}")
      template_dir = Path.dirname(path)
      cond do
        :modified in events -> get_initial_state(template_dir)
        :removed in events -> get_initial_state(template_dir)
        :created in events -> get_initial_state(template_dir)
        :renamed in events -> get_initial_state(template_dir)
        true -> Enum.each(events, fn(event) -> Logger.info("Didn't handle file event " <> to_string(event)) end )
      end
    else
      Logger.info("The unchecked path is #{path}")
    end

    {:noreply, source}
  end

  def handle_info(_, options) do
    {:noreply, options}
  end

  def test() do
    "This is a test"
  end
end
