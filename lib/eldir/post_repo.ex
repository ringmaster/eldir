require Logger
require IEx


defmodule Eldir.PostRepo do
  use GenServer

  alias Eldir.Post, as: Post

  def start_link(options) do
    GenServer.start_link(__MODULE__, options)
  end

  def init(options) do
    :ets.new(:posts, [:ordered_set, :public, :named_table])
    :ets.new(:categories, [:bag, :public, :named_table])
    :ets.new(:authors, [:bag, :public, :named_table])
    :ets.new(:files, [:bag, :public, :named_table])

    :ok = watch(options[:source])

    GenServer.cast(self(), :reset)

    {:ok, options}
  end

  def handle_cast(:reset, state) do
    state[:source] |> get_initial_state

    {:noreply, state}
  end

  ## Initialize
  defp get_initial_state(source) do
    start = Timex.now()
    posts = read_all_posts(source)

    Logger.debug("Compiled #{Enum.count(posts)} posts in #{Timex.diff Timex.now(), start, :milliseconds}ms.")
  end

  defp read_all_posts(source) do
    source
      |> File.ls!
      |> Flow.from_enumerable(max_demand: 1)
      |> Flow.map(&(Path.join([source, &1])))
      |> Flow.filter(&(Path.extname(&1) == ".md"))
      |> Flow.map(&Post.transform/1)
      # |> Flow.partition
      |> Flow.filter(fn(item)-> match?({:ok, _}, item) end)
      |> Flow.map(fn({:ok, item}) -> item end)
      |> Flow.each(&upsert_by_post/1)
      |> Enum.to_list()
  end

  ## Retrieval functions
  def page(posts, page, per_page \\ 5) do
    posts
    |> Enum.chunk_every(per_page)
    |> Enum.at(page, [])
  end

  def list do
    :ets.match(:posts, {:_, :"$1"})
      |> List.flatten
      |> Enum.reverse
  end

  def categories do
    :ets.match(:categories, {:"$1", :_})
      |> List.flatten
      |> Enum.reduce(%{}, fn(item, acc) ->
        Map.update(acc, item, 1, fn(x) -> x + 1 end)
      end)
      |> (fn(a) -> {:ok, a} end).()
  end

  def get_by_slug(slug) do
    case :ets.match(:posts, {{:_, slug}, :"$1"}) |> List.flatten do
      [post | _] -> {:ok, post}
      _ -> :not_found
    end
  end

  def get_by_category(category) do
    :ets.match(:categories, {category, :"$1"})
      |> List.flatten
      |> Enum.map(fn(dateslug) ->
        [{_, post} | _] = :ets.lookup(:posts, dateslug) |> List.flatten
        post
      end)
      |> (fn(a) -> {:ok, a} end).()
  end

  def get_by_author(author) do
    :ets.match(:authors, {author, :"$1"})
      |> List.flatten
      |> Enum.map(fn(dateslug) ->
        [{_, post} | _] = :ets.lookup(:posts, dateslug) |> List.flatten
        post
      end)
      |> (fn(a) -> {:ok, a} end).()
  end

  def get_by_filename(filename) do
    :ets.match(:files, {filename, :"$1"})
      |> List.flatten
      |> Enum.map(fn(dateslug) ->
        [{_, post} | _] = :ets.lookup(:posts, dateslug) |> List.flatten
        post
      end)
      |> (fn(a) -> if a == [], do: {:noresult, a}, else: {:ok, a} end).()
  end

  ## File handling functions
  def update_by_file_name(filename) do
    if File.exists?(filename) do
      Logger.debug("Updating #{filename} into post")
      case Post.transform(filename) do
        {:ok, post} -> upsert_by_post(post)
      end
    else
      Logger.debug("Deleting any post for #{filename}")
      case get_by_filename(filename) do
        {:ok, [post]} ->
          post_slug = date_slug(post)
          Logger.debug("Deleting post #{post.slug} from deleted file #{filename}")
          :ets.delete(:posts, post_slug)
          # Need to figure out how to remove the values from :categories, :authors, and :files
        _ ->
          Logger.debug("Deleted file #{filename} has no post record to delete")
      end
    end
  end

  ## Utility functions
  defp date_slug(%Post{} = post) do
    {NaiveDateTime.to_erl(post.date), post.slug}
  end

  ## Post struct handling functions
  def upsert_by_post(post) do
    #Logger.debug("Upserting #{post.slug} from #{post.file_name}")

    date_slug = date_slug(post)
    :ets.insert(:posts, {date_slug, post})
    post.categories
      |> Enum.each(fn(category) ->
        :ets.insert(:categories, {category, date_slug})
      end)
    post.authors
      |> Enum.each(fn(author) ->
        :ets.insert(:authors, {author, date_slug})
      end)
    :ets.insert(:files, {post.file_name, date_slug})
    post
  end

  ## Watching function message handling
  def watch(source) do
    Logger.info("Watching #{source} for post updates...")
    name = to_string(__MODULE__) <> ".FS" |> String.to_atom

    {:ok, _pid} = :fs.start_link(name, source)
    :fs.subscribe(name)
  end

  def handle_info({_pid, {:fs, :file_event}, {path, events}}, source) do
    if Path.extname(path) == ".md" do
      Logger.debug(Enum.join(Enum.map(events, fn i -> Atom.to_string(i) end), "+") <>  " change detected in #{path}")
      cond do
        :modified in events -> update_by_file_name(path)
        :removed in events -> update_by_file_name(path)
        :created in events -> update_by_file_name(path)
        :renamed in events -> update_by_file_name(path)
        true -> Enum.each(events, fn(event) -> Logger.info("Didn't handle file event " <> to_string(event)) end )
      end
    else
      Logger.info("The unchecked path is #{path}")
    end

    {:noreply, source}
  end

  def handle_info(_, options) do
    {:noreply, options}
  end
end
