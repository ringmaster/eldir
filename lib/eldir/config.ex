defmodule Eldir.Config do
  use GenServer
  require Logger

  def start_link(options) do
    GenServer.start_link(__MODULE__, options, name: __MODULE__)

    posts_dir = Path.join([options[:source], get("repos.posts.path")])
    template_dir = Path.join([options[:source], get("template.path")])

    # List all child processes to be supervised
    children = [
      # Start the endpoint when the application starts
      {Eldir.PostRepo, [source: posts_dir]},
      {Eldir.Templates, [source: template_dir, routes: get("routes")]}
      # Starts a worker by calling: Eldir.Worker.start_link(arg)
      # {Eldir.Worker, arg},
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Eldir.ConfigSupervisor]
    Supervisor.start_link(children, opts)
  end

  def init(options) do
    {:ok, options |> get_initial_state}
  end

  def get_initial_state(options) do
    {:ok, toml} = Path.join([options[:source], "config.toml"]) |> Toml.decode_file()
    Keyword.put(options, :toml, toml)
  end

  def handle_call(:get_toml, _from, value) do
    {:reply, value[:toml], value}
  end

  def handle_call(:get_options, _from, options) do
    posts_dir = Path.join([options[:source], options[:toml]["repos"]["posts"]["path"]])
    template_dir = Path.join([options[:source], options[:toml]["template"]["path"]])
    value_out = options
    |> Keyword.put(:posts_dir, posts_dir)
    |> Keyword.put(:template_dir, template_dir)
    {:reply, value_out, options}
  end

  def options() do
    GenServer.call(Eldir.Config, :get_options)
  end

  def get(index, default \\ nil) do
    toml = GenServer.call(__MODULE__, :get_toml)
    get_in(toml, index |> String.split(".")) || default
  end

  def get_config_base() do
    GenServer.call(__MODULE__, :get_toml)
    |> Enum.reduce([], fn {key, value}, acc -> [{String.to_atom(key), value} | acc] end)
    |> Enum.filter(fn({_,x}) -> !is_map(x) end)
  end

  def page_number(params) do
    if params["page"], do: String.to_integer(params["page"]), else: 1
  end

  def get_assigns(params) do
    # all_posts = Eldir.PostRepo.list()
    # post = case Eldir.PostRepo.get_by_slug("success-with-phoenix") do
    #   {:ok, value} ->
    #     value
    #   _ ->
    #     %Eldir.Post{}
    # end

    params
    |> Map.put("config", get_config_base())
    |> Map.put("page", page_number(params))
    # |> Map.put("pages", ceil(Enum.count(all_posts) / get("paginate", 5)))
    # |> Map.put("posts", Eldir.PostRepo.page(all_posts, page_number(params) - 1, get("paginate", 5)))
    # |> Map.put("post", post)
    |> Map.put("layout", "layout.html")
    |> Map.put("template", "posts.html")
    |> Enum.map(fn({key, value}) -> {String.to_atom(key), value} end)
  end

end
