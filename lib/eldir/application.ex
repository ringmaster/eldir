defmodule Eldir.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  @root_dir       File.cwd!
  @repo_dir       Path.join(~w(#{@root_dir} repo))

  use Application

  def start(_type, _args) do
    repo_dir = System.get_env("REPO_DIR", @repo_dir)

    # List all child processes to be supervised
    children = [
      # Start the endpoint when the application starts
      EldirWeb.Endpoint,
      {Eldir.Config, [source: repo_dir]}
      # Starts a worker by calling: Eldir.Worker.start_link(arg)
      # {Eldir.Worker, arg},
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Eldir.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    EldirWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
