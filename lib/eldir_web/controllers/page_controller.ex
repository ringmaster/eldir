require IEx

defmodule EldirWeb.PageController do
  use EldirWeb, :controller

  def index(conn, params) do
    assigns = Eldir.Config.get_assigns(params)
    conn
    |> Eldir.Templates.Current.route(assigns[:path], assigns)
    #|> Map.put(:resp_headers, [{"content-type", "application/rss"}])
    #|> IO.inspect()
  end
end
